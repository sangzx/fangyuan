package com.fangyuan.model;

import lombok.Data;
import org.assertj.core.util.Lists;

import java.util.List;

/**
 *  小区 community
 *     地址  address
 *     户型  Door model
 *     平方  square
 *     楼层  floor
 *     层高  The_height
 *     地铁  The_subway
 *     朝向   toward
 *     是否整租  Whether_TheWhole_Rent
 *     来源   source
 *     联系人  The_Contact
 *     联系电话  Contact_Phone_Number
 */
@Data
public class Fangyuan {
   private String community;
    private String address;
    private String doorModel;
    private String square;
    private String floor;
    private String theHeight;
    private String theSubway;
    private String  toward;
    private String  whetherTheWholeRent;
    private String  source;
    private String  theContact;
    private String  contactPhoneNumber;

    public List<String> getParam(){
     return Lists.newArrayList(this.community,this.address,this.doorModel,this.square,
             this.floor,this.theHeight,this.theSubway,this.toward,this.whetherTheWholeRent,this.source,
      this.theContact,this.contactPhoneNumber);
    }
}

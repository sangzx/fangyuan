package com.fangyuan.anjuke;

import com.fangyuan.model.Fangyuan;
import com.fangyuan.util.DataSource;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import java.sql.Connection;
import java.util.List;

public class AnJuKePipeline implements Pipeline {

    public void process(ResultItems resultItems, Task task) {
        List<Fangyuan> fangyuans = resultItems.get("fangyuans");
        Connection connection = DataSource.getConnection();
        for (Fangyuan fangyuan:
        fangyuans) {
            DataSource.execute(fangyuan.getParam(),connection);
        }
        DataSource.close(connection);
    }
}
